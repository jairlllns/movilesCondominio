package com.example.estudiante.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void irQuejas(View vista){
        Intent intent= new Intent(this, Quejas.class);
        startActivity(intent);

    }

    public void irArea(View vista){
        Intent intent= new Intent(this, PrestamoArea.class);
        startActivity(intent);

    }

    public void irParqueadero(View vista){
        Intent intent= new Intent(this, Parqueadero.class);
        startActivity(intent);

    }

    public void irPago(View vista){
        Intent intent= new Intent(this, PagoCondominio.class);
        startActivity(intent);

    }

    public void irPropietarios(View vista){
        Intent intent= new Intent(this, ListaPropietarios.class);
        startActivity(intent);

    }

    public void irMenuArea(View vista){
        Intent intent= new Intent(this, MenuArea.class);
        startActivity(intent);

    }
}
